interface Regtangle {
    width: number;
    height: number;
}

interface ColoredRegtangle extends Regtangle {
    color: String
}

const regtangle: Regtangle = {
    width: 20,
    height: 10
}

console.log(regtangle);

const coloredRegtangle: ColoredRegtangle = {
    width: 20,
    height: 10,
    color: "red"
}
console.log(coloredRegtangle)